#ifndef TEMPL_NONCOPYABLE_H
#define TEMPL_NONCOPYABLE_H


class NonCopyable
{
public:
    NonCopyable() = default;

    NonCopyable(const NonCopyable& other) = delete;
    NonCopyable& operator=(const NonCopyable& other) = delete;

    virtual ~NonCopyable() = default;
};


#endif //TEMPL_NONCOPYABLE_H
