#include <iostream>
#include "SoundDevice.h"
#include "pocketsphinx/pocketsphinx.h"
#define MODELDIR "/home/dwhys/templ/libs/pocketsphinx-5prealpha/model"
int32_t main()
{
    CaptureSoundDevice captureDevice(CAPTURE_SOUND_DEVICE_CONFIG);

    Status captureDeviceInitResult = captureDevice.Init();
    if(captureDeviceInitResult.Failed()) { return captureDeviceInitResult.Code(); }

    std::vector<std::string> capturedData {};

    std::cout << "Capturing...\n";

    captureDevice.Capture(5, capturedData);

    std::cout << "Capturing is finished\n";

    sleep(4);

    PlaybackSoundDevice playbackDevice(PLAYBACK_SOUND_DEVICE_CONFIG);

    Status playbackDeviceInitResult = playbackDevice.Init();
    if(playbackDeviceInitResult.Failed()) { return playbackDeviceInitResult.Code(); }

    playbackDevice.Playback(capturedData);

    ps_decoder_t *ps = nullptr;
    cmd_ln_t *config = nullptr;

    config = cmd_ln_init(nullptr, ps_args(), TRUE,
                            "-hmm", MODELDIR "/en-us/en-us",
                            "-lm", MODELDIR "/en-us/en-us.lm.bin",
                            "-dict", MODELDIR "/en-us/cmudict-en-us.dict",
                            nullptr);

    if (config == nullptr)
    {
        std::cout << "Failed to create config object, see log for details\n";
        return -1;
    }

    int32_t operationResult = ps_start_utt(ps);
    for(const auto& period : capturedData)
    {
        operationResult = ps_process_raw(ps, reinterpret_cast<const int16 *>(period.data()), period.size(), FALSE, FALSE);
        if(operationResult == 0)
        {
            std::cout << "Error while processing captured data.\n";
        }
    }

    operationResult = ps_end_utt(ps);
    int32_t score {};
    const char* hypothesis = ps_get_hyp(ps, &score);
    printf("Recognized: %s\n", hypothesis);

    return 0;
}

