#ifndef TEMPL_SOUNDDEVICE_H
#define TEMPL_SOUNDDEVICE_H

#include <cstdint>
#include <alsa/asoundlib.h>
#include <memory>
#include <vector>
#include "NonCopyable.h"
#include "SoundDeviceConfig.h"
#include "Status.h"


class SoundDevice : public NonCopyable
{
public:
    explicit SoundDevice(const SoundDeviceConfig& config);
    ~SoundDevice() override;

public:
    Status Init();

protected:
    snd_pcm_t* hDevice_;
    snd_pcm_hw_params_t* deviceParameters_;
    snd_pcm_stream_t streamType_;
    snd_pcm_access_t pcmAccessMode_;
    snd_pcm_format_t dataFormatMode_;
    uint32_t channels_;
    uint32_t sampleRate_;
    snd_pcm_uframes_t periodSize_;
    uint32_t periodTime_;
    int32_t subUnitDirection_;
};


class CaptureSoundDevice : public SoundDevice
{
public:
    explicit CaptureSoundDevice(const SoundDeviceConfig& config);
    ~CaptureSoundDevice() override = default;
    
public:
    Status Capture(const uint32_t seconds, std::vector<std::string>& capturedData);
};

class PlaybackSoundDevice : public SoundDevice
{
public:
    explicit PlaybackSoundDevice(const SoundDeviceConfig& config);
    ~PlaybackSoundDevice() override = default;
    
public:
    Status Playback(const std::vector<std::string>& data);
};












#endif //TEMPL_SOUNDDEVICE_H
