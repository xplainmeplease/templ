#include <iostream>
#include "SoundDevice.h"

SoundDevice::SoundDevice(const SoundDeviceConfig& config)
        : NonCopyable()
        , hDevice_{nullptr}
        , deviceParameters_{nullptr}
        , streamType_{config.streamType}
        , pcmAccessMode_{config.pcmAccessMode}
        , dataFormatMode_{config.dataFormatMode}
        , channels_{config.channels}
        , sampleRate_{config.sampleRate}
        , periodSize_{config.periodSize}
        , periodTime_{0}
        , subUnitDirection_{0}
{}


SoundDevice::~SoundDevice()
{
    snd_pcm_close(hDevice_);
}


Status SoundDevice::Init()
{
    Status status {};

    int32_t result = snd_pcm_open(&hDevice_, "default", streamType_, 0);
    if(result < 0)
    {
        std::cout << "Unable to open PCM device\n";
        status = Status::Error::PcmDeviceInitError;
        return status;
    }

    snd_pcm_hw_params_alloca(&deviceParameters_);

    //Fill with default values
    snd_pcm_hw_params_any(hDevice_, deviceParameters_);

    result = snd_pcm_hw_params_set_format(hDevice_, deviceParameters_,SND_PCM_FORMAT_S16_LE);
    if(result < 0)
    {
        std::cout << "Setting format error\n";
        status = Status::Error::HardwareParametersSetFormatError;
        return status;
    }

    result = snd_pcm_hw_params_set_channels(hDevice_, deviceParameters_,2);
    if(result < 0)
    {
        std::cout << "Setting channels number error\n";
        status = Status::Error::HardwareParametersSetChannelsNumberError;
        return status;
    }

    result = snd_pcm_hw_params_set_rate_near(hDevice_, deviceParameters_, &sampleRate_, &subUnitDirection_);
    if(result < 0)
    {
        std::cout << "Setting sample rate error\n";
        status = Status::Error::HardwareParametersSetSampleRateError;
        return status;
    }

    result = snd_pcm_hw_params_set_period_size_near(hDevice_, deviceParameters_, &periodSize_, &subUnitDirection_);
    if(result < 0)
    {
        std::cout << "Setting sample rate error\n";
        status = Status::Error::HardwareParametersSetSampleRateError;
        return status;
    }

    result =  snd_pcm_hw_params(hDevice_, deviceParameters_);
    if(result < 0)
    {
        std::cout << "Setting parameters to a device error\n";
        status = Status::Error::ApplyParametersToDeviceError;
        return status;
    }

    result = snd_pcm_hw_params_get_period_time(deviceParameters_,&periodTime_, &subUnitDirection_);
    if(result < 0)
    {
        std::cout << "Getting period time from a device\n";
        status = Status::Error::ApplyParametersToDeviceError;
        return status;
    }

    return status;

}

Status CaptureSoundDevice::Capture(const uint32_t seconds, std::vector<std::string>& capturedData)
{
    Status status {};

    int32_t captureResult {};

    uint32_t loopsForCapture = seconds * 1'000'000 / periodTime_;

    const uint32_t FRAME_BUFFER_SIZE = periodSize_ * 4;

    auto frameBuffer = new char[FRAME_BUFFER_SIZE];

    while(loopsForCapture > 0)
    {
        captureResult = snd_pcm_readi(hDevice_, frameBuffer, periodSize_);
        if( captureResult == -EPIPE)
        {
            std::cout << "Overrun occurred while playing data.\n";
            status = Status::Error::CaptureOverrun;
            return status;
        }
        else if ( captureResult < 0  || captureResult != static_cast<int32_t>(periodSize_))
        {
            std::cout << "Capture error\n";
            status = Status::Error::PlaybackError;
            return status;
        }

        capturedData.emplace_back(frameBuffer, FRAME_BUFFER_SIZE);

        --loopsForCapture;
    }

    delete[] frameBuffer;

    return status;
}

CaptureSoundDevice::CaptureSoundDevice(const SoundDeviceConfig &config)
        : SoundDevice(config)
{
}

Status PlaybackSoundDevice::Playback(const std::vector<std::string> &data)
{
    Status status {};

    int32_t playbackResult {};

    for(const auto &period : data)
    {
        playbackResult = snd_pcm_writei(hDevice_, period.data(), periodSize_);
        if( playbackResult == -EPIPE)
        {
            std::cout << "Underrun occurred while playing data.\n";
            status = Status::Error::PlaybackUnderrun;
            return status;
        }
        else if ( playbackResult < 0  || playbackResult != static_cast<int32_t>(periodSize_))
        {
            std::cout << "Playback error\n";
            status = Status::Error::PlaybackError;
            return status;
        }
    }

    return status;
}

PlaybackSoundDevice::PlaybackSoundDevice(const SoundDeviceConfig &config)
        : SoundDevice(config)
{
}
