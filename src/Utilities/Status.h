#ifndef TEMPL_STATUS_H
#define TEMPL_STATUS_H


#include <cstdint>

class Status
{
public:
    using CodeType = int32_t;

    enum class Success : CodeType
    {
        Success = 0,
    };
    enum class Error : CodeType
    {
        PcmDeviceInitError = -1,
        HardwareParametersSetFormatError = -2,
        HardwareParametersSetChannelsNumberError = -3,
        HardwareParametersSetSampleRateError = -4,
        ApplyParametersToDeviceError = -5,
        CaptureOverrun = -6,
        PlaybackUnderrun = -7,
        PlaybackError = -8,



    };

public:
    constexpr Status() noexcept : code_{ 0 } {};
    explicit constexpr Status(int32_t code) noexcept : code_{code} {}
    explicit constexpr Status(Error   code) noexcept : Status{static_cast<int32_t>(code)} {}
    explicit constexpr Status(Success code) noexcept : Status{static_cast<int32_t>(code)} {}
    ~Status() noexcept = default;

    explicit constexpr operator bool() const noexcept { return code_ == static_cast<int32_t>(Success::Success); }

    constexpr Status& operator=(Error code) noexcept { return *this = Status{ code }; }
    constexpr Status& operator=(Success code) noexcept { return *this = Status{ code }; }

    constexpr CodeType Code() const noexcept { return code_; }

    constexpr bool Succeed() const noexcept { return code_ == static_cast<int32_t>(Success::Success); }
    constexpr bool Failed()  const noexcept { return code_ < static_cast<int32_t>(Success::Success); }

private:
    CodeType code_;
};

constexpr bool operator==(Status lhs, Status rhs) noexcept { return lhs.Code() == rhs.Code(); }
constexpr bool operator==(Status lhs, Status::Error   rhs) noexcept { return lhs == Status { rhs }; }
constexpr bool operator==(Status lhs, Status::Success rhs) noexcept { return lhs == Status { rhs }; }
constexpr bool operator==(Status::Error   lhs, Status rhs) noexcept { return Status { lhs } == rhs; }
constexpr bool operator==(Status::Success lhs, Status rhs) noexcept { return Status { lhs } == rhs; }

constexpr bool operator!=(Status lhs, Status rhs) noexcept { return !(lhs == rhs); }
constexpr bool operator!=(Status lhs, Status::Error   rhs) noexcept { return !(lhs == Status { rhs }); }
constexpr bool operator!=(Status lhs, Status::Success rhs) noexcept { return !(lhs == Status { rhs }); }
constexpr bool operator!=(Status::Error   lhs, Status rhs) noexcept { return !(Status { lhs } == rhs); }
constexpr bool operator!=(Status::Success lhs, Status rhs) noexcept { return !(Status { lhs } == rhs); }

#endif //TEMPL_STATUS_H
