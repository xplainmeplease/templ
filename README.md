# Implementation plan

1. Create capture/playback devices, make them working independently.(Firstly save capture, then play it after N seconds)
2. Wrap it using OOP.
3. Compare data recieved via ALSA with data used inside Pocketsphinx



# Pattern explanation

- \w - word
- \W - not word
- \d - digit character
- \D - not a digit character
- \s - whitespaces
- \S - not whitespaces
- .  - dot
- +  - 1 or more of preceding token
- *  - 0 or more of preceding token
- {1,3} - 1 to 3 of preceding token
- ?  - 0 or 1 of preceding token
- |  - boolean 0

# Use cases

- Say phrase: `Скопируй все слова из файла, которые начинаются на букву {letter}`
- Say phrase: `Скопируй все слова из файла, без пунктуации`
- Say phrase: `Скопируй все слова из файла`
- Say phrase: `Скопируй слово из файла, в котором 4 буквы`



# Modules

```
/modules
	-----| 
	-----| RegexPatternRecognizer
	
```


