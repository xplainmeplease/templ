#include <alsa/asoundlib.h>
#include <string>
#include <iostream>


void LogRecordedData(const std::string & data)
{
    std::cout << "Recorded Data: \n\t\t\t";
    for(const auto& it : data)
    {
        std::cout << static_cast<int32_t>(it) << " ";
    }

    std::cout << std::endl;
}


int main() {
    SoundData soundData {};
    long loops;
    int rc;
    int size;
    snd_pcm_t *captureHandle;
    snd_pcm_hw_params_t *captureParams;
    unsigned int val;
    int dir;
    snd_pcm_uframes_t frames;
    char *captureBuffer;

    /* Open PCM device for recording (capture). */
    rc = snd_pcm_open(&captureHandle, "default",
                      SND_PCM_STREAM_CAPTURE, 0);
    if (rc < 0) {
        fprintf(stderr,
                "unable to open pcm device: %s\n",
                snd_strerror(rc));
        exit(1);
    }

    /* Allocate a hardware parameters object. */
    snd_pcm_hw_params_alloca(&captureParams);

    /* Fill it in with default values. */
    snd_pcm_hw_params_any(captureHandle, captureParams);

    /* Set the desired hardware parameters. */

    /* Interleaved mode */
    snd_pcm_hw_params_set_access(captureHandle, captureParams,
                                 SND_PCM_ACCESS_RW_INTERLEAVED);

    /* Signed 16-bit little-endian format */
    snd_pcm_hw_params_set_format(captureHandle, captureParams,
                                 SND_PCM_FORMAT_S16_LE);

    /* Two channels (stereo) */
    snd_pcm_hw_params_set_channels(captureHandle, captureParams, 2);

    /* 44100 bits/second sampling rate (CD quality) */
    val = 44100;
    snd_pcm_hw_params_set_rate_near(captureHandle, captureParams,
                                    &val, &dir);

    /* Set period size to 32 frames. */
    frames = 32;
    snd_pcm_hw_params_set_period_size_near(captureHandle,
                                           captureParams, &frames, &dir);

    /* Write the parameters to the driver */
    rc = snd_pcm_hw_params(captureHandle, captureParams);
    if (rc < 0) {
        fprintf(stderr,
                "unable to set hw parameters: %s\n",
                snd_strerror(rc));
        exit(1);
    }

    /* Use a captureBuffer large enough to hold one period */
    snd_pcm_hw_params_get_period_size(captureParams,
                                      &frames, &dir);
    size = frames * 4; /* 2 bytes/sample, 2 channels */
    captureBuffer = (char *) malloc(size);

    /* We want to loop for 5 seconds */
    snd_pcm_hw_params_get_period_time(captureParams,
                                      &val, &dir);
    loops = 5'000'000 / val;

    while (loops > 0) {
        loops--;
        rc = snd_pcm_readi(captureHandle, captureBuffer, frames);
        if (rc == -EPIPE) {
            /* EPIPE means overrun */
            fprintf(stderr, "overrun occurred\n");
            snd_pcm_prepare(captureHandle);
        } else if (rc < 0) {
            fprintf(stderr,
                    "error from read: %s\n",
                    snd_strerror(rc));
        } else if (rc != (int)frames) {
            fprintf(stderr, "short read, read %d frames\n", rc);
        }

        std::string tmp(captureBuffer, size);
        soundData.data.push_back(tmp);
        LogRecordedData(tmp);
    }

    std::cout << "Finish capturing\n";

//    snd_pcm_drain(captureHandle);
    snd_pcm_close(captureHandle);
    free(captureBuffer);

    std::cout << "Create playback device\n";

    snd_pcm_t *playbackHandle;
    snd_pcm_hw_params_t *playbackParams;

    /* Open PCM device for playback. */
    rc = snd_pcm_open(&playbackHandle, "default",
                      SND_PCM_STREAM_PLAYBACK, 0);
    if (rc < 0) {
        fprintf(stderr,
                "unable to open pcm device: %s\n",
                snd_strerror(rc));
        exit(1);
    }

    /* Allocate a hardware parameters object. */
    snd_pcm_hw_params_alloca(&playbackParams);

    /* Fill it in with default values. */
    snd_pcm_hw_params_any(playbackHandle, playbackParams);

    /* Set the desired hardware parameters. */

    /* Interleaved mode */
    snd_pcm_hw_params_set_access(playbackHandle, playbackParams,
                                 SND_PCM_ACCESS_RW_INTERLEAVED);

    /* Signed 16-bit little-endian format */
    snd_pcm_hw_params_set_format(playbackHandle, playbackParams,
                                 SND_PCM_FORMAT_S16_LE);

    /* Two channels (stereo) */
    snd_pcm_hw_params_set_channels(playbackHandle, playbackParams, 2);

    /* 44100 bits/second sampling rate (CD quality) */
    val = 44100;
    snd_pcm_hw_params_set_rate_near(playbackHandle, playbackParams,
                                    &val, &dir);

    /* Set period size to 32 frames. */
    frames = 32;
    snd_pcm_hw_params_set_period_size_near(playbackHandle,
                                           playbackParams, &frames, &dir);

    /* Write the parameters to the driver */
    rc = snd_pcm_hw_params(playbackHandle, playbackParams);
    if (rc < 0) {
        fprintf(stderr,
                "unable to set hw parameters: %s\n",
                snd_strerror(rc));
        exit(1);
    }

    /* Use a buffer large enough to hold one period */
    snd_pcm_hw_params_get_period_size(playbackParams, &frames,
                                      &dir);
    /* We want to loop for 5 seconds */
    snd_pcm_hw_params_get_period_time(playbackParams,
                                      &val, &dir);

    for(auto& it: soundData.data)
    {
        rc = snd_pcm_writei(playbackHandle, it.data(), frames);
        if (rc == -EPIPE) {
            /* EPIPE means underrun */
            fprintf(stderr, "underrun occurred\n");
            snd_pcm_prepare(playbackHandle);
        } else if (rc < 0) {
            fprintf(stderr,
                    "error from writei: %s\n",
                    snd_strerror(rc));
        }  else if (rc != (int)frames) {
            fprintf(stderr,
                    "short write, write %d frames\n", rc);
        }
    }

    std::cout << "finishing\n";
    snd_pcm_drain(playbackHandle);
    snd_pcm_close(playbackHandle);

    return 0;
}
