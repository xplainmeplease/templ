#ifndef TEMPL_SOUNDDEVICECONFIG_H
#define TEMPL_SOUNDDEVICECONFIG_H

#include <alsa/asoundlib.h>

constexpr uint8_t FRAMES_IN_PERIOD = 32;
constexpr uint8_t CHANNELS_STEREO = 2;
constexpr uint32_t SAMPLE_RATE = 44100;

struct SoundDeviceConfig
{
    snd_pcm_stream_t streamType;
    snd_pcm_access_t pcmAccessMode;
    snd_pcm_format_t dataFormatMode;
    uint32_t channels;
    uint32_t sampleRate;
    snd_pcm_uframes_t periodSize;
};

constexpr SoundDeviceConfig PLAYBACK_SOUND_DEVICE_CONFIG {
    SND_PCM_STREAM_PLAYBACK,
    SND_PCM_ACCESS_RW_INTERLEAVED,
    SND_PCM_FORMAT_S16_LE,
    CHANNELS_STEREO,
    SAMPLE_RATE,
    FRAMES_IN_PERIOD,
};

constexpr SoundDeviceConfig CAPTURE_SOUND_DEVICE_CONFIG {
    SND_PCM_STREAM_CAPTURE,
    SND_PCM_ACCESS_RW_INTERLEAVED,
    SND_PCM_FORMAT_S16_LE,
    CHANNELS_STEREO,
    SAMPLE_RATE,
    FRAMES_IN_PERIOD,
};


#endif //TEMPL_SOUNDDEVICECONFIG_H
